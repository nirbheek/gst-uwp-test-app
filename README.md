## What is this?

This is a modification of the 'hello-world' C++/CX UWP template to play
gstreamer pipelines and output the debug log in a user-accessible location.

## Usage

After building GStreamer UWP binaries using Cerbero, open `hello-world.sln` in
VS 2017 or newer, select the configuration you want to build, and build/deploy
it (Local Machine, emulators didn't work for me). Note that to test for WACK,
you need release binaries.

Under `Assets`, you will find filters that add all the DLLs required by this
hello world app (and more) which will be automatically updated and copied into
the app.

`MainPage.xaml/MainPage.xaml.cpp` is where all the code is. The rest should be
self-explanatory, there's nothing special here.
