﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gio/gio.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>

#include <locale>
#include <codecvt>
#include <string>

using namespace hello_world;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Core;
using namespace Windows::Storage;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

GstElement *pipe = NULL;
FILE *log_file = NULL;
SwapChainPanel^ video_output;
TextBlock^ pipe_status;
TextBlock^ log_output;
CoreDispatcher^ dispatcher;

gchar *_app_gst_plugin_list[] = {
  { "gstcoreelements.dll" },
  { "gstapp.dll" },
  { "gstaudiotestsrc.dll" },
  { "gstaudioconvert.dll" },
  { "gstaudioresample.dll" },
  { "gstaudiorate.dll" },
  { "gstcompositor.dll" },
  { "gstisomp4.dll" },
  { "gstlibav.dll" },
  { "gstopengl.dll" },
  { "gstproxy.dll" },
  { "gstplayback.dll" },
  { "gstvideotestsrc.dll" },
  { "gstvideoconvert.dll" },
  { "gstvideorate.dll" },
  { "gstvideoscale.dll" },
  { "gstwasapi.dll" },
  { "gstx264.dll" },
};

String^ gchar_to_platform_string (const gchar * s)
{
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
  std::wstring wide = converter.from_bytes (s);
  return ref new Platform::String (wide.c_str ());
}

gchar* platform_string_to_gchar (String ^ s)
{
  std::wstring ws (s->Data ());
  return g_utf16_to_utf8 ((const gunichar2*) ws.c_str (), -1, NULL, NULL, NULL);
}

void _gst_log_handler_file (GstDebugCategory *category, GstDebugLevel level, const gchar *file, const gchar *function, gint line, GObject * obj, GstDebugMessage * message, gpointer user_data)
{
  gchar *msg;
  
  msg = g_strdup_printf ("%s %20s %s:%d:%s:%s\n", gst_debug_level_get_name (level),
    gst_debug_category_get_name (category), file, line, function,
    gst_debug_message_get (message));
  fputs (msg, log_file);
  fflush (log_file);
  g_free (msg);
}
/*
void _g_log_handler_app (const gchar * log_domain, GLogLevelFlags log_level, const gchar * message, gpointer user_data)
{
  dispatcher->RunAsync (CoreDispatcherPriority::Normal,
    ref new DispatchedHandler ([message]()
  {
    log_output->Text = log_output->Text + gchar_to_platform_string (message) + "\n";
  }));
}
*/
void _set_status (gchar * msg)
{
  pipe_status->Text = gchar_to_platform_string (msg);
}

void _set_error (GError ** error)
{
  if (!error)
    return;
  _set_status ((*error)->message);
  g_clear_error (error);
}

gboolean _gst_load_plugins (void)
{
  int i;
  GError *error = NULL;
  GstPlugin *plugin;
  GstRegistry *reg = gst_registry_get ();

  for (i = 0; i < G_N_ELEMENTS (_app_gst_plugin_list); i++) {
    plugin = gst_plugin_load_file (_app_gst_plugin_list[i], &error);
    if (!plugin) {
      _set_error (&error);
      return FALSE;
    }

    if (!gst_registry_add_plugin (reg, plugin)) {
      gst_object_unref (plugin);
      _set_status ("Failed to add plugin to registry");
      return FALSE;
    }
    gst_object_unref (plugin);
  }
  return TRUE;
}

void setup_log_handlers (const gchar * appdir)
{
  gst_debug_remove_log_function (gst_debug_log_default);
  gst_debug_set_default_threshold (GST_LEVEL_LOG);

  gchar *log_filename;
  log_filename = g_strdup_printf ("%s\\gst.log", appdir);
  errno_t err = fopen_s (&log_file, log_filename, "w");

  if (!log_file) {
    g_free (log_filename);
    _set_status (strerror (err));
    return;
  }

  gst_debug_add_log_function (_gst_log_handler_file, NULL, NULL);

  // Output the log file location
  {
    gchar *m = g_strdup_printf ("Writing log to %s", log_filename);
    log_output->Text = gchar_to_platform_string (m);
    g_free (m);
  }
  g_free (log_filename);

  //g_log_set_default_handler (_g_log_handler_app, NULL);
}

MainPage::MainPage()
{
  gchar *appdir;
  GError *error = NULL;

	InitializeComponent();

  video_output = pipeOverlay;
  log_output = logOutput;
  pipe_status = statusOutput;
  //dispatcher = CoreWindow::GetForCurrentThread ()->Dispatcher;
  appdir = platform_string_to_gchar (ApplicationData::Current->LocalFolder->Path);

  setup_log_handlers (appdir);

  gst_init (NULL, NULL);
  if (!_gst_load_plugins ()) {
    return;
  }

  //"audiotestsrc samplesperbuffer=4410 num-buffers=100 ! queue ! wasapisink "
#define MP4_PIPELINE  "qtmux name=mux ! filesink name=fsink "\
  "compositor name=c ! queue ! x264enc tune=zerolatency ! avdec_h264 ! x264enc tune=zerolatency ! mux. " \
  "videotestsrc num-buffers=300 ! video/x-raw,width=320,height=240 ! c. " \
  "videotestsrc num-buffers=300 pattern=ball ! video/x-raw,width=160,height=120 ! c. "
#define FAKESINK_PIPELINE "audiotestsrc ! queue ! wasapisink " \
  "compositor name=c ! queue ! fakesink sync=true " \
  "videotestsrc pattern=ball ! queue ! c. " \
  "videotestsrc ! queue ! c. "
#define GLMIX_MP4_PIPELINE \
  "glvideomixer name=c ! queue ! gldownload ! x264enc tune=zerolatency ! qtmux ! filesink name=fsink " \
  "gltestsrc num-buffers=300 ! video/x-raw(memory:GLMemory),width=320,height=240  ! c. " \
  "gltestsrc num-buffers=300 pattern=mandelbrot ! video/x-raw(memory:GLMemory),width=160,height=120 ! c. "
#define GLSINK_PIPELINE "audiotestsrc ! queue ! wasapisink " \
  "gltestsrc ! queue ! glimagesink name=overlay "

  // Change this to set what to run
#define PIPELINE FAKESINK_PIPELINE
  pipe = gst_parse_launch (PIPELINE, &error);
  if (!pipe) {
    _set_error (&error);
    g_free (appdir);
    return;
  }


  if (g_strcmp0 (PIPELINE, MP4_PIPELINE) == 0 ||
      g_strcmp0 (PIPELINE, GLMIX_MP4_PIPELINE) == 0) {
    GstElement *fsink = gst_bin_get_by_name (GST_BIN (pipe), "fsink");
    gchar *out_mp4 = g_strdup_printf ("%s\\out.mp4", appdir);
    g_object_set (fsink, "location", out_mp4, NULL);
    g_object_unref (fsink);
  }

  g_free (appdir);
}

void hello_world::MainPage::Button_Click (Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
  greetingOutput->Text = "Hello, " + nameInput->Text + "!";
}

void hello_world::MainPage::Play_Pipeline (Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
  GstState state;
  GstStateChangeReturn ret;

  if (!pipe)
    return;

  if (pipe_status->Text == "PLAYING") {
    ret = gst_element_set_state (pipe, GST_STATE_PAUSED);
    if (ret == GST_STATE_CHANGE_FAILURE) {
      pipe_status->Text = "STATE CHANGE FAILURE";
      return;
    }
  } else {
    // Set the SwapChainPanel element as the overlay for glimagesink to draw onto
    // NOTE: This currently always draws black for some reason
    if (g_strcmp0 (PIPELINE, GLSINK_PIPELINE) == 0) {
      GstElement *overlay = gst_bin_get_by_name (GST_BIN (pipe), "overlay");
      gst_video_overlay_set_window_handle (GST_VIDEO_OVERLAY (overlay), (guintptr) reinterpret_cast<IInspectable*>(pipeOverlay));
      g_object_unref (overlay);
    }

    ret = gst_element_set_state (pipe, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
      pipe_status->Text = "STATE CHANGE FAILURE";
      return;
    }
  }

  // Reset the button text
  playButton->Content = "Play";

  // XXX: Do this asynchronously with the app threading model
  gst_element_get_state (pipe, &state, NULL, 5 * GST_MSECOND);
  switch (state) {
    case GST_STATE_PLAYING:
      pipe_status->Text = "PLAYING";
      playButton->Content = "Pause";
      break;
    case GST_STATE_PAUSED:
      pipe_status->Text = "PAUSED";
      break;
    case GST_STATE_READY:
      pipe_status->Text = "READY";
      playButton->Content = "Recheck Status";
      break;
    case GST_STATE_NULL:
      pipe_status->Text = "NULL!";
      break;
    default:
      pipe_status->Text = "UNKNOWN";
  }
}
